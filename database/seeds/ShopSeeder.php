<?php

    use Illuminate\Database\Seeder;
    use Illuminate\Support\Facades\DB;

    class ShopSeeder extends Seeder
    {
        /**
         * Run the database seeds.
         *
         * @return void
         */
        public function run()
        {
            // only one record so we'll do it old school way
            DB::table('shops')->insert([
                'name' => 'Main Store'
            ]);
        }
    }
