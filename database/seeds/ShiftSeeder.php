<?php

    use App\Rota;
    use App\Staff;
    use Carbon\Carbon;
    use Illuminate\Database\Seeder;
    use Illuminate\Support\Facades\DB;

    class ShiftSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        // only one record so we'll do it old school way
        DB::table('shifts')->insert([[
            'rota_id' => Rota::first()->id,
            'staff_id' => Staff::first()->id,
            'start_time' => Carbon::parse('2020-06-15 09:00'),
            'end_time' => Carbon::parse('2020-06-15 17:00')
        ], [
            'rota_id' => Rota::first()->id,
            'staff_id' => Staff::skip(1)->first()->id,
            'start_time' => Carbon::parse('2020-06-15 08:00'),
            'end_time' => Carbon::parse('2020-06-15 16:00')
        ]]);
    }
}
