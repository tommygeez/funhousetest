<?php

    use App\Shop;
    use Carbon\Carbon;
    use Illuminate\Database\Seeder;
    use Illuminate\Support\Facades\DB;

    class RotaSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        // only one record so we'll do it old school way
        DB::table('rotas')->insert([
            'shop_id' => Shop::first()->id,
            'week_commence_date' => Carbon::parse('2020-06-15')
        ]);
    }
}
