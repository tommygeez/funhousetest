<?php

    use Illuminate\Database\Seeder;

    class DatabaseSeeder extends Seeder
    {
        /**
         * Seed the application's database.
         *
         * @return void
         */
        public function run()
        {
            $this->call(ShopSeeder::class);
            $this->call(StaffSeeder::class);
            $this->call(RotaSeeder::class);
            $this->call(ShiftSeeder::class);
        }
    }
