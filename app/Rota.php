<?php

namespace App;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Collection;

/**
 * Class Rota
 * @package App
 */
class Rota extends Model
{

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function shop()
    {
        return $this->belongsTo(Shop::class);
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function shifts()
    {
        return $this->hasMany(Shift::class);
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function staff()
    {
        return $this->belongsToMany(Staff::class, 'shifts');
    }

    /**
     * @return array
     */
    public function getMinutes(): array
    {
        $daysToIncrement = 7; // monday-sunday
        $dayOfRotaBegins = Carbon::parse($this->week_commence_date);
        $singleManningMinutesPerDay = [];

        for ($i = 0; $i < $daysToIncrement; ++$i) {

            if (0 == $i) {
                // this is day 1 of rota
                $dayStart = Carbon::parse($this->week_commence_date)->startOfDay();
                $dayEnd = $dayStart->copy()->endOfDay();

            } else {
                // add one day to the first day
                $dayStart = $dayOfRotaBegins->copy()->addDays($i)->startOfDay();
                $dayEnd = $dayStart->copy()->endOfDay();

            }

            // get shifts for a given day
            $shifts = Shift::whereBetween(
                'start_time',
                [$dayStart, $dayEnd]
            )->get();

            $singleManningMinutesPerDay[$dayStart->format('D')] =
                $this->getSingleManningMinutesFromDay($shifts);

        }

        return $singleManningMinutesPerDay;
    }

    /**
     * @param Collection $shifts
     * @return int
     */
    private function getSingleManningMinutesFromDay(Collection $shifts): int
    {
        // if just one person works entire day
        if ($shifts->count() == 1) {
            $shift = $shifts->first();
            $startTime = Carbon::parse($shift->start_time);
            $endTime = Carbon::parse($shift->end_time);

            return $startTime->diffInMinutes($endTime);
        }

        $minutesWorkedAlone = 0;

        // iterate over shifts and find overlapping shifts
        $shifts->map(function ($shift) use (&$minutesWorkedAlone) {

            // we get duration of this shift
            $startTime = Carbon::parse($shift->start_time);
            $endTime = Carbon::parse($shift->end_time);

            // now check if there's any other shift that starts or ends during this shift
            $overlappingShift = Shift::where(
                'id', '!=', $shift->id
            )->whereBetween(
                'start_time', [$startTime, $endTime]
            )->orWhereBetween(
                'end_time', [$startTime, $endTime]
            )->where('id', '!=', $shift->id
            )->first();

            // Note - ABOVE - not sure why it still includes current shift even tho it has id !=
            // just left it like this for the time being, works with two where ID !=,
            // someone smarter will surely point out what's wrong =)

            $otherStaffStart = Carbon::parse($overlappingShift->start_time);
            $otherStaffEnd = Carbon::parse($overlappingShift->end_time);

            // if the other shift started earlier
            if ($startTime->greaterThan($otherStaffStart)) {
                // minutes worked alone between his end of work and mine
                $minutesWorkedAlone += ($otherStaffEnd->diffInMinutes($endTime));
            }

            if ($startTime->lessThan($otherStaffStart)) {
                // minutes worked alone between my start and his start
                $minutesWorkedAlone += ($otherStaffStart->diffInMinutes($startTime));
            }

            // if we started work together
            if ($startTime->equalTo($otherStaffStart)) {
                if ($otherStaffEnd->lessThan($endTime)) {
                    // his work day is shorter than mine
                    $minutesWorkedAlone += ($otherStaffEnd->diffInMinutes($endTime));
                }
            }

        });

        return $minutesWorkedAlone;
    }
}
