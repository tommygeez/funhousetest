<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Staff extends Model
{
    protected $table = 'staff';

    public function shop()
    {
        return $this->belongsTo(Shop::class);
    }

    public function rotas()
    {
        return $this->belongsToMany(Rota::class, 'shifts');
    }

    public function currentRota($rotaId = null)
    {
        return $this->belongsToMany(Rota::class, 'shifts')->wherePivot('rota_id', $rotaId);
    }

    public function shifts()
    {
        return $this->belongsToMany(Rota::class, 'shifts')->wherePivot('staff_id', $this->id)->withPivot(['start_time', 'end_time', 'id']);
    }
}
