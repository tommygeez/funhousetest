<?php

    namespace App\Http\Controllers;

    use App\Rota;
    use FunHouse\IsStaffWorkingAloneShift;
    use FunHouse\ShiftMinutesCalculator;
    use FunHouse\SingleManningMinutes;

    class ExampleController extends Controller
    {
        public function index()
        {
            $rota = Rota::firstOrFail();
            var_dump(SingleManningMinutes::fromModel($rota));
        }
    }
