<?php

    namespace FunHouse;

    use App\Rota;
    use Spatie\DataTransferObject\DataTransferObject;

    /**
     * Class SingleManningMinutes
     * @package FunHouse
     */
    final class SingleManningMinutes extends DataTransferObject
    {

        /**
         * @var
         */
        public $manningMinutesWeek; // add type in PHP 7.4 which I didn't have

        /**
         * @param Rota $rota
         * @return SingleManningMinutes
         */
        public static function fromModel(Rota $rota): SingleManningMinutes
        {
            return new static([
                'manningMinutesWeek' => $rota->getMinutes()
            ]);
        }
    }
