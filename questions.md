How long did you spend doing the test?

-- I've spent 7 hours in total, unfortunately had teething baby crying all the time hence I had to take constant breaks to help... I also had issues setting up laravel on my local computer which took another chunk of time. Coding itself took me around 2 Hours and 1 Hour thinking how this all should work

What would I add to my solution if I had more time?

-- Many things, I'd write proper salary calculations with bonuses and break time. I'd spend a little bit of time refactoring the code as I'm 100% sure it can be written in an easier way / cleaner. I tried to stick to 4H window coding wise hence not all covered. I also had to learn about DTO within those 4 Hours, not really used that in the past.

Why did you choose PHP as your programming language?

-- I didn't choose it, it chose me. Was an accident on the interview, since then I've learned to love it =)

Favourite thing about Laravel?

-- The fact that it's Laravel. Best framework in the world in my opinion, easy to use/learn, very well written, stable, powerful and fun to work with. Documentation is so great it makes me want to sing

Least favourite thing about Laravel?

-- Nothing


Final notes:

-- Apologies If I haven't shown full power here, in general coding is much more fun, quicker and cleaner. Looks like few months on furlough have made me forget bits n pieces.

The main logis is within src directory and Rota model within apps
The call to SingleManningMinutes is within ExampleController


If you're willing to test the code I've created Example controller which you can visit by going to /about page, the output should be there without any errors. Please just migrate and seed the database before doing so. I've made seeder for shop, rota, staff and shifts for you.
